#Decison tree with Iris data set
install.packages("party")
iris <- read.csv("E:/MCA IV/Data Mininig/Data Mining Lab/Lab 3 k means/iris.csv")
str(iris)
set.seed(1234)

#here the nrow gives the whole the rows. Need not necesssay to have prob. It varies

ind<-sample(2,nrow(iris),replace=TRUE,prob = NULL)

View(ind)
nrow(iris)

traindata<-iris[ind==1,]
View(traindata)
testdata<-iris[ind==2,]
View(testdata)
library(party)
myFormula<-Species~S.Length+S.Width+P.Length+P.Width
table(myFormula)
#ctree build the decision tree
iris_ctree<-ctree(myFormula,data=traindata)
# predict make the predictions
table(predict(iris_ctree),traindata$Species)
print(iris_ctree)
plot(iris_ctree)
plot(iris_ctree,type="simple")
testPred<-predict(iris_ctree,newdata=testdata)
table(testPred,testdata$Species)


#Decision tree with weather data set with party package
weather <- read.csv("E:/MCA IV/Data Mininig/Data Mining Lab/Decison tree/weather.csv")
View(weather)
str(weather)
set.seed(1234)
ind<-sample(2,nrow(weather),replace=TRUE,prob=c(0.7,0.3))
View(ind)

Wtraindata<-weather[ind==1,]
View(Wtraindata)
Wtestdata<-weather[ind==2,]
View(Wtestdata)

library(party)
myFormula<-play~outlook+temperature+humidity+windy
#ctree build the decision tree
weather_ctree<-ctree(myFormula,data=Wtraindata)
weather_ctree

# predict make the predictions
table(predict(weather_ctree),Wtraindata$play)
print(weather_ctree)
plot(weather_ctree)
plot(weather_ctree,type="simple")

WtestPred<-predict(weather_ctree,newdata=Wtestdata)
table(WtestPred,Wtestdata$play)
print(WtestPred)
plot(WtestPred)
plot(WtestPred,type="simple")

# with package C50
library(C50)
weather <- read.csv("E:/MCA IV/Data Mininig/Data Mining Lab/Decison tree/weather.csv")
View(weather)
weather$play<-as.factor(weather$play)
weather$windy<-as.factor(weather$windy)
str(weather)
table(weather$outlook)
table(weather$temperature)
table(weather$humidity)
table(weather$windy)
summary(weather$play)
set.seed(1234)
ind<-sample(2,nrow(weather),replace=TRUE,prob=c(0.7,0.3))
View(ind)

Wtraindata<-weather[ind==1,]
View(Wtraindata)
Wtestdata<-weather[ind==2,]
View(Wtestdata)
prop.table(table(Wtraindata$play))
prop.table(table(Wtestdata$play))

myFormula=play~outlook+temperature+humidity+windy
myFormula
library(C50)
weather_tree<-C5.0(Wtraindata,Wtraindata$play)
weather_tree
plot(weather_tree)
# predict make the predictions
trainPred<-predict((weather_tree),Wtraindata)
table(trainPred)
print(trainPred)
plot(trainPred)

weather_pred<-predict(weather_tree,Wtestdata)
weather_pred
plot(weather_pred)

library(gmodels)
CrossTable(Wtestdata$play,weather_pred, prop.chisq = FALSE, prop.r = FALSE, dnn=c('actual play','predicted play'))


# predict make the predictions
table(predict(weather_ctree),Wtraindata$play)
trainPred<-table(predict(weather_ctree),Wtraindata$play)
print(trainPred)

WtestPred<-predict(weather_ctree,newdata=Wtestdata)
table(WtestPred,Wtestdata$play)
print(WtestPred)
plot(WtestPred)


#with R Weka
library(RWeka)
weather <- read.csv("E:/MCA IV/Data Mininig/Data Mining Lab/Decison tree/weather.csv")

View(weather)
weather$play<-as.factor(weather$play)
str(weather)
set.seed(1234)
ind<-sample(2,nrow(weather),replace=TRUE,prob=c(0.7,0.3))
View(ind)

Wtraindata<-weather[ind==1,]
View(Wtraindata)
Wtestdata<-weather[ind==2,]
View(Wtestdata)

myFormula<-play~outlook+temperature+humidity+windy
#ctree build the decision tree
weather_ctree<-J48(myFormula,Wtraindata)
weather_ctree
plot(weather_ctree)
print(weather_ctree)
plot(weather_ctree,type="simple")


#generate the rules
JRip(myFormula,Wtraindata)

# predict make the predictions
table(predict(weather_ctree),Wtraindata$play)
trainPred<-table(predict(weather_ctree),Wtraindata$play)
print(trainPred)
trainPred


#generate the rules

WtestPred<-predict(weather_ctree,newdata=Wtestdata)
table(WtestPred,Wtestdata$play)
print(WtestPred)
plot(WtestPred)
